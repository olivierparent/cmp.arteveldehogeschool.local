#!/usr/bin/env bash

echo Installing WordPress '$1'
WP_DIRECTORY=`dirname $0`/wordpress
git clone --depth 1 -b "$1" https://github.com/WordPress/WordPress $WP_DIRECTORY
rm -rf $WP_DIRECTORY/.git
